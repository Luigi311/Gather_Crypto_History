import argparse, os
from CoinbaseFunction import coinbase_csv

parser = argparse.ArgumentParser(
    description="Create CSV of all transactions from multiple exchanges")

parser.add_argument("--source", type=str,
                    help="Specify a specific source only")

args = parser.parse_args()

Output_folder = "Output/"
if not os.path.exists(Output_folder):
  os.makedirs(Output_folder)

delta_struc = ["Date", "Type", "Exchange", "Base amount", "Base currency", "Quote amount", "Quote currency", "Fee",
               "Fee currency", "Costs/Proceeds", "Costs/Proceeds currency", "Sync Holdings", "Sent/Received from","Sent to", "Notes"]

coinbase_api_file = "API_KEYS/Coinbase.key"

try:
  coinbase_csv(delta_struc, coinbase_api_file, Output_folder)

except KeyboardInterrupt:
  print('\nExiting')
